%clear all
%clc;

r = 4;
x = [0 0 2*sqrt(3)];
y = [3 7 5];

pc = [1 6 3];

for  i=1:1:3
    
z(i) = pc(3) + sqrt(r^2-(y(i) - pc(2))^2 - (x(i) - pc(1))^2);

end

col1 = line([x(1) x(1)], [y(1) y(1)], [0 8]);
col2 = line([x(2) x(2)], [y(2) y(2)], [0 8]);
col3 = line([x(3) x(3)], [y(3) y(3)], [0 8]);

arm1 = line([x(1) pc(1)], [y(1) pc(2)], [z(1) pc(3)]);
arm2 = line([x(2) pc(1)], [y(2) pc(2)], [z(2) pc(3)]);
arm3 = line([x(3) pc(1)], [y(3) pc(2)], [z(3) pc(3)]);

%view(30,6)
hold on
plot3(pc(1), pc(2), pc(3), 'o')
lk = [];
k = 1;
text = 'x1 y1 z1';
%s1 = serial('/dev/ttyUSB1','BaudRate',57600);
%fopen(s1);


for t= 0:0.1:100
    pc(1) = sin(t) + sqrt(3);
    pc(2) = cos(t) + 5;
    pc(3) = abs(4 * sin(t / 20));
    
    for  i=1:1:3
       z(i) = pc(3) + sqrt(r^2-(y(i) - pc(2))^2 - (x(i) - pc(1))^2);
    end
    set(arm1, 'xData', [x(1) pc(1)], 'yData', [y(1) pc(2)], 'zData', [z(1) pc(3)]);
    set(arm2, 'xData', [x(2) pc(1)], 'yData', [y(2) pc(2)], 'zData', [z(2) pc(3)]);
    set(arm3, 'xData', [x(3) pc(1)], 'yData', [y(3) pc(2)], 'zData', [z(3) pc(3)]);
    
    if(exist('z0'))
        lk = [lk ; z - z0];
        k = k +1;
        text = ['x ' num2str(round(lk(k - 1, 1)*1000)) ' y ' num2str(round(lk(k -1, 2) * 1000)) ' z ' num2str(round(lk(k -1, 3) * 1000))]
    end
    
    fprintf(s1,text);
    text
    while(true)
        if s1.BytesAvailable > 1
            while s1.BytesAvailable
               fscanf(s1);
            end
            break;
        end
    end
    
    %text = ['x ' num2str(lk(1,k-1)) ' y ' num2str(lk(2,k-1)) ' z ' num2str(lk(3,k-1))];
     z0 = z;
    plot3(pc(1), pc(2), pc(3), 'o')
    drawnow;
end

fclose(s1);
  
for i=1:length(lk)

end